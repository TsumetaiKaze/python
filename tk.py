import tkinter, time

gui = tkinter.Tk()
gui.geometry("1200x800")
gui.title("My App")

col = "black"
def coler():
    global col
    if col=="black":
        col = "red"
    else:
        col = "black"
    return col

frOne = tkinter.Frame(gui,width=200, height=800, bg="grey50")
frOne.pack(side="left", padx=20)

myClock = tkinter.Label(frOne, text='Current time',font="Arial 14 bold italic")
myClock.pack(pady=50)

def worldClock():
    global myClock
    txtClock = time.strftime("%H:%M:%S\n%d %m %Y")
    myClock.destroy()
    myClock = tkinter.Label(frOne, text=txtClock,font="Arial 14 bold italic", bg=col)
    myClock.pack(pady=50)
    gui.after(1000,worldClock)

worldClock()

btnStart = tkinter.Button(frOne, width=15, height=2, command=coler, text="Нажми меня!!!")
btnStart.pack()

canv = tkinter.Canvas(gui, width=1000, height=800, bg="gray20")
canv.pack()

gui.mainloop()
