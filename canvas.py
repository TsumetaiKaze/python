import tkinter, random

gui = tkinter.Tk()
gui.geometry("1200x800")
gui.title("My App")

canv = tkinter.Canvas(gui, width=1000, height=750, bg="gray20")
canv.pack()

dataSet = [random.randint(400,600) for i in range(60)]

def draw(ds=dataSet):
    XY = []
    x = 0
    for i in ds:
        XY.append(x)
        y = 750-i
        XY.append(y)
        canv.create_oval(x-7, y-7, x+7, y+7, fill="", outline="yellow")
        x = x+20
    canv.create_line(XY, fill="yellow", width=2)

btnStart = tkinter.Button(gui, width=15, height=2, text="Нажми меня!!!", command=draw)
btnStart.pack()

gui.mainloop()
