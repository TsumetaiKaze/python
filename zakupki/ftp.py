from ftplib import FTP
 
ftp = FTP('ftp.cse.buffalo.edu')
print(ftp.login())

data = ftp.retrlines('LIST') 
print(data)

# Меняем директорию
ftp.cwd('pub')
 
data = ftp.retrlines('LIST')
print(data)

ftp.cwd('ubuntu-releases')
print(data)
 
# Путь на нашем компьютере где сохранить файл.
out = 'README.html'
 
with open(out, 'wb') as f:
    ftp.retrbinary('RETR ' + 'README.html', f.write)
