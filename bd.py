import random, tkinter, csv, sqlite3

surnames = []

with open("lastnames.csv", "r", newline="") as sn:
    names = csv.reader(sn)
    for i in names:
        surnames.append(i)

firstnames = []

with open("americanames600.csv", "r", newline="") as fn:
    names = csv.reader(fn)
    for i in names:
        firstnames.append(i)

address = []

with open("american-address-Fake.csv", "r", newline="") as add:
    adds = csv.reader(add, delimiter=";")
    for i in adds:
        address.append(i)

base = sqlite3.connect('amerwork.db')
cur = base.cursor()
cur.execute("""CREATE table if not exists works(
numid int primary key,
ageworker int,
name text,
lastname text,
skill text,
adrs text,
state text,
city text);
""")
base.commit()

skills = ['врач','строитель','ученый','плотник','медсестра','моляр']

workers = []
for i in range(0,199):
    ageworker = random.randint(18,50)
    name = random.choice(firstnames)
    name = name[0]
    lastname = random.choice(surnames)
    lastname = lastname[0]
    skill = random.choice(skills)
    adrs = random.choice(address)
    state = adrs[2]
    city = adrs[3]
    adrs = adrs[0]
    workers.append((i+1,ageworker,name,lastname,skill,adrs,state,city))
#print(workers)
#for i in workers:
#    cur.execute("""insert into works VALUES(?,?,?,?,?,?,?,?);""",i)
#base.commit()

rec = cur.execute("""select * from works""")
for i in rec:
    print(i)

def selSlill(inSkill):
    print(inSkill)
    rec = cur.execute("""select * from works where skill like '?'""",inSkill)
    kolvo = 0
    for i in rec:
        print(i)
        kolvo += 1
    print('всего {} {}',kolvo,inSkill)

selSlill('плотник')
base.close()
