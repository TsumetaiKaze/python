"""
2. В списке trade содержатся статистические данные о торговле по тюменской области
в следующем порядке:
Год
Сумма продаж (млн. руб.) продовольственных товаров
Сумма продаж (млн. руб.) непродовольственных товаров
Требуется:
1. Составить линейные диаграммы по каткегориям динамики за 4 года 
2. Составить круговые диаграммы по каждому году
"""
trade = [["Year 2016",159184,174624],
["Year 2017",169698,184437],
["Year 2018",179345,204433],
["Year 2019",188856,213258]]

prodTrade = []
noProdTrade = []
for i in trade:
    prodTrade.append(i[1])
    noProdTrade.append(i[2])

import tkinter

gui = tkinter.Tk()
gui.geometry("1280x800")
gui.title("Trade")
gui["bg"] = "gray40"
w = 1000 #ширина canvas
h = 800  #высота canvas

frBtn = tkinter.Frame(gui, bg="grey50")
frBtn.pack(side="left", padx=20)

canv = tkinter.Canvas(gui, width=w, height=h, bg="gray20")
canv.pack(side="left")

def Year(ind):
    canv.delete("all")
    total = trade[ind][1]+trade[ind][2]#Это суммарный товарооборот
    scale = total/360#сумма на один градус
    prod = trade[ind][1]/scale#сколько градусов не продовольствие
    noProd = trade[ind][2]/scale#сколько градусов продовольствие
    x = w/2
    y = h/2
    begin = 0
    coler = 505
    for i in [prod,noProd]:
        d = "#"+str(coler)
        canv.create_arc(x-150, y-150, x+150, y+150, extent=i, start=begin, style="arc", width=15, outline=d)
        begin = begin+i
        coler = coler+90

def Year2016():
    Year(0)

def Year2017():
    Year(1)

def Year2018():
    Year(2)

def Year2019():
    Year(3)

def drawDyn(inList,step,scale,color):
    x = step
    XY = []
    for i in inList:
        XY.append(x)
        y = h-(i*scale)
        XY.append(y)
        canv.create_oval(x-7, y-7, x+7, y+7, fill="", outline=color)
        x = x+step
    canv.create_line(XY, fill=color, width=2)

def Dynamic():
    canv.delete("all")
    scale = (h-100)/max(noProdTrade)
    step = w/6
    drawDyn(prodTrade,step,scale,"green")
    drawDyn(noProdTrade,step,scale,"red")


btn2016 = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="2016", command=Year2016)
btn2016.pack(sid="top", padx=15, pady=10)
btn2017 = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="2017", command=Year2017)
btn2017.pack(sid="top", padx=15, pady=10)
btn2018 = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="2018", command=Year2018)
btn2018.pack(sid="top", padx=15, pady=10)
btn2019 = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="2019", command=Year2019)
btn2019.pack(sid="top", padx=15, pady=10)
btnDynamic = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="Динамика", command=Dynamic)
btnDynamic.pack(sid="top", padx=15, pady=10)

gui.mainloop()
