"""
1. В списке hokkey содержатся данные 61 хоккеиста КХЛ в следующем порядке
порядковый номер
Имя Фамилия
Амплуа
Количество забитых шайб
Количество результативных передач
Количество сыгранных матчей
Штрафное время в минутах. 
Требуется:
а) Построить столбчатую диаграмму 20-ти самых результативных нападающих
б) Построить столбчатую диаграмму 20-ти самых недисциплинированных хоккеистов
г) Построить линейную диаграмму результативных передач нападающих
д) Построить линейную диаграмму результативных передач защитников
е) Построить столбчатую диаграмму по соотношению штрафного времени на сумму голов и передач
"""
hokkey = [
['1', 'Кирилл Петров  ', 'Н', '18', '15', '56', '22'],   
['2', 'Никита Точицкий  ', 'Н', '7', '26', '60', '24'],  
['3', 'Лукаш Радил  ', 'Н', '19', '13', '58', '47'],     
['4', 'Никлас Енсен  ', 'Н', '19', '13', '58', '43'],    
['5', 'Станислав Галиев  ', 'Н', '19', '13', '60', '18'],
['6', 'Итан Уэрек  ', 'Н', '15', '17', '53', '42'],      
['7', 'Антон Ландер  ', 'Н', '14', '18', '60', '28'],
['9', 'Игорь Гераськин  ', 'Н', '11', '21', '62', '14'],
['10', 'Дмитрий Кугрышев  ', 'Н', '10', '22', '66', '45'],
['11', 'Брукс Мэйсек  ', 'Н', '19', '12', '62', '44'],
['12', 'Кирилл Марченко  ', 'Н', '17', '14', '51', '16'],
['13', 'Олег Ли  ', 'Н', '17', '14', '56', '36'],
['14', 'Владимир Бутузов  ', 'Н', '15', '16', '60', '16'],
['15', 'Сергей Мозякин  ', 'Н', '14', '17', '49', '4'],
['16', 'Робин Ганзл  ', 'Н', '12', '19', '52', '30'],
['17', 'Артур Каюмов  ', 'Н', '12', '19', '68', '27'],
['18', 'Кертис Волк  ', 'Н', '10', '21', '62', '69'],
['19', 'Юусо Пуустинен  ', 'Н', '18', '12', '46', '14'],
['20', 'Антон Слепышев  ', 'Н', '14', '16', '46', '8'],
['21', 'Хантер Шинкарук  ', 'Н', '12', '18', '47', '10'],
['22', 'Мэттью Фрэттин  ', 'Н', '12', '18', '55', '22'],
['23', 'Кирилл Семенов  ', 'Н', '12', '18', '71', '47'],
['25', 'Лоуренс Пилут  ', 'З', '7', '23', '62', '38'],
['26', 'Мартин Бакош  ', 'Н', '15', '14', '46', '18'],
['27', 'Павел Порядин  ', 'Н', '14', '15', '49', '25'],
['28', 'Брендэн Лайпсик  ', 'Н', '13', '16', '48', '24'],
['29', 'Алексей Бывальцев  ', 'Н', '13', '16', '63', '56'],
['30', 'Артем Федоров  ', 'Н', '12', '17', '60', '42'],
['31', 'Роман Старченко  ', 'Н', '15', '20', '65', '20'],
['32', 'Егор Яковлев  ', 'З', '10', '20', '63', '28'],
['33', 'Никита Сошников  ', 'Н', '8', '20', '52', '16'],
['34', 'Юусо Хиетанен  ', 'З', '7', '20', '65', '18'],
['35', 'Даниил Тарасов  ', 'Н', '19', '19', '65', '33'],
['36', 'Андре Петерссон  ', 'Н', '18', '19', '53', '30'],
['37', 'Николай Прохоркин  ', 'Н', '15', '19', '57', '50'],
['38', 'Михал Чайковски  ', 'З', '13', '19', '68', '46'],
['39', 'Робин Ганзл  ', 'Н', '12', '19', '52', '30'],
['40', 'Артур Каюмов  ', 'Н', '12', '19', '68', '27'],
['41', 'Даниил Мироманов  ', 'З', '10', '19', '58', '16'],
['42', 'Алексей Марченко  ', 'З', '8', '19', '69', '20'],
['43', 'Юрки Йокипакка  ', 'З', '5', '19', '59', '12'],
['44', 'Антон Бурдасов  ', 'Н', '25', '18', '57', '14'],
['45', 'Джордан Шредер  ', 'Н', '18', '18', '54', '14'],
['46', 'Маттиас Теденбю  ', 'Н', '17', '18', '54', '30'],
['47', 'Антон Ландер  ', 'Н', '14', '18', '60', '28'],
['48', 'Хантер Шинкарук  ', 'Н', '12', '18', '47', '10'],
['49', 'Мэттью Фрэттин  ', 'Н', '12', '18', '55', '22'],
['50', 'Кирилл Семенов  ', 'Н', '12', '18', '71', '47'],
['51', 'Егор Морозов  ', 'Н', '11', '18', '57', '28'],
['52', 'Хенрик Хаапала  ', 'Н', '8', '18', '52', '24'],
['53', 'Данила Моисеев  ', 'Н', '8', '18', '56', '16'],
['54', 'Дэн Секстон  ', 'Н', '7', '18', '41', '10'],
['55', 'Лукас Бенгтссон  ', 'З', '5', '18', '49', '18'],
['56', 'Никита Михайлис  ', 'Н', '20', '17', '65', '47'],
['57', 'Иван Чехович  ', 'Н', '19', '17', '47', '18'],
['58', 'Йоонас Кемппайнен  ', 'Н', '18', '17', '64', '12'],
['59', 'Итан Уэрек  ', 'Н', '15', '17', '53', '42'],
['60', 'Сергей Мозякин  ', 'Н', '14', '17', '49', '4'],
['61', 'Артем Федоров  ', 'Н', '12', '17', '60', '42']]
"""
2. В списке trade содержатся статистические данные о торговле по тюменской области
в следующем порядке:
Год
Сумма продаж (млн. руб.) продовольственных товаров
Сумма продаж (млн. руб.) непродовольственных товаров
Требуется:
1. Составить линейные диаграммы по каткегориям динамики за 4 года 
2. Составить круговые диаграммы по каждому году
"""
trade = [["Year 2016",159184,174624],
["Year 2017",169698,184437],
["Year 2018",179345,204433],
["Year 2019",188856,213258]]

fr = []
fr_send = []
def_send = []
fines = []
hk =[]
for i in hokkey:
    fines.append((int(i[-1]),i[1]))
    s = int(i[3])+int(i[4])
    if s!=0:
        s = int(i[-1])/s
    s = round(s,2)
    hk.append((s,i[1]))
    if i[2]=="Н":
        fr.append((int(i[3]),i[1]))
        fr_send.append((int(i[4]),i[1]))
    else:
        def_send.append((int(i[4]),i[1]))
fr.sort(reverse=True)
fr_send.sort(reverse=True)
def_send.sort(reverse=True)
bombers = fr[:20]
fines.sort(reverse=True)
fines = fines[:20]
hk.sort()

import tkinter

gui = tkinter.Tk()
gui.geometry("1280x800")
gui.title("CHL")
gui["bg"] = "gray40"
w = 1000 #ширина canvas
h = 800  #высота canvas

frBtn = tkinter.Frame(gui, bg="grey50")
frBtn.pack(side="left", padx=20)

canv = tkinter.Canvas(gui, width=w, height=h, bg="gray20")
canv.pack(side="left")

def bombardir():#лушие
    canv.delete("all")
    scale = (h-80)/bombers[0][0]
    step = round(w/len(bombers))
    x = step/2
    a = True
    for i in bombers:
        y = i[0]*scale
        y = h-y
        canv.create_line(x, h, x, y, fill="green3", width=40)
        canv.create_text(x, y+5, text=i[0], font="Arial 14", fill="white", anchor="n")
        if a:
            canv.create_text(x, 5, text=i[1], font="Arial 14", fill="white", anchor="n")
        else:
            canv.create_text(x, 30, text=i[1], font="Arial 14", fill="white", anchor="n")
        a = not a
        x = x+step

def fine():#штрафники
    canv.delete("all")
    scale = (h-80)/fines[0][0]
    step = round(w/len(fines))
    x = step/2
    a = True
    for i in fines:
        y = i[0]*scale
        y = h-y
        canv.create_line(x, h, x, y, fill="green3", width=40)
        canv.create_text(x, y+5, text=i[0], font="Arial 14", fill="white", anchor="n")
        if a:
            canv.create_text(x, 5, text=i[1], font="Arial 14", fill="white", anchor="n")
        else:
            canv.create_text(x, 30, text=i[1], font="Arial 14", fill="white", anchor="n")
        a = not a
        x = x+step

def forwards():#напдающие
    canv.delete("all")
    scale = (w-100)/fr_send[0][0]
    step = round((h-10)/len(fr_send))
    y = 20
    for i in fr_send:
        x = i[0]*scale
        canv.create_line(150, y, x, y, fill="green3", width=4)
        canv.create_text(x, y, text=i[0], font="Arial 14", fill="white", anchor="w")
        canv.create_text(150, y-5, text=i[1], font="Arial 8", fill="white", anchor="e")
        y = y+step

def defenders():#защитники
    canv.delete("all")
    scale = (w-100)/def_send[0][0]
    step = round((h-10)/len(def_send))
    y = 20
    for i in def_send:
        x = i[0]*scale
        canv.create_line(150, y, x, y, fill="green3", width=4)
        canv.create_text(x, y, text=i[0], font="Arial 14", fill="white", anchor="w")
        canv.create_text(150, y-5, text=i[1], font="Arial 8", fill="white", anchor="e")
        y = y+step

def relfine():#время
    canv.delete("all")
    scale = (h-80)/hk[-1][0]
    step = round(w/len(hk))
    x = step/2
    a = True
    for i in hk:
        y = i[0]*scale
        y = h-y
        canv.create_line(x, h, x, y, fill="green3", width=step-4)
        canv.create_text(x, y-5, text=i[0], font="Arial 14", fill="white", anchor="s")
        if a:
            canv.create_text(x, 5, text=i[1], font="Arial 14", fill="white", anchor="s")
        else:
            canv.create_text(x, 30, text=i[1], font="Arial 14", fill="white", anchor="s")
        a = not a
        x = x+step    

btnBomb = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="20-ти самых\nрезультативных\nнападающих", command=bombardir)
btnBomb.pack(sid="top", padx=15, pady=10)

btnFine = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="20-ти самых\nнедисциплинированных\nхоккеистов", command=fine)
btnFine.pack(sid="top", padx=15, pady=10)

btnForw = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="результативные\nпередачи\nнападающих", command=forwards)
btnForw.pack(sid="top", padx=15, pady=10)

btnDef = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="результативные\nпередачи\nзащитников", command=defenders)
btnDef.pack(sid="top", padx=15, pady=10)

btnDF = tkinter.Button(frBtn, bg="royalblue", width=20, height=3, padx=5, relief="flat", bd=0,
                          fg="white", text="соотношение штрафного\nвремени на сумму\nголов и передач", command=relfine)
btnDF.pack(sid="top", padx=15, pady=10)

gui.mainloop()
