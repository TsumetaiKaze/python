import csv,tkinter,random

dataAir = []#аэропорты

with open("Airports.csv", "r", newline="") as ap:
    ports = csv.reader(ap, delimiter=";")
    for i in ports:
        dataAir.append(i)
        print(i)

height = []
for i in dataAir:
    print(i[-1])
    if i[-1]=="N/Aft":
        continue
    ha = i[-1][:-2]
    ha = int(ha)
    ha = ha*0.3048
    ha = round(ha,2)
    print(ha)
    height.append((ha,i[1]))
height.sort(reverse=True)
for i in height:
    print(i)

gui = tkinter.Tk()
gui.geometry("1280x1000")
gui.title("CHL")
gui["bg"] = "gray40"
w = 1100 #ширина canvas
h = 900  #высота canvas

frBtn = tkinter.Frame(gui, bg="grey50")
frBtn.pack(side="left", padx=20)

canv = tkinter.Canvas(gui, width=w, height=h, bg="gray20")
canv.pack(side="left")

scale = (w-180)/height[0][0]
#print(scale)
step = round((h-5)/len(height))
#print(step)
y = step
for i in height:
    x = i[0]*scale
    x = 110+x
    print(x)
    canv.create_line(110, y, x, y, fill="green3", width=4)
    canv.create_text(x, y, text=i[0], font="Arial 14", fill="white", anchor="w")
    canv.create_text(110, y, text=i[1], font="Arial 10", fill="white", anchor="e")
    y = y+step

debit = ["Smith","Johnson","Williams","Jones","Brown","Davis","Miller","Wilson","Moore","Taylor"]

dataSet = []
for i in debit:
    subData = []
    subData.append(i)
    a = random.randint(500,1200)
    subData.append(a)
    b = random.randint(5,24)
    subData.append(b)
    c = random.randint(80,97)
    subData.append(c)
    d = random.randint(200,500)
    subData.append(d)
    dataSet.append(subData)
print(dataSet)

with open("DataSet.csv", "w", newline="") as ds:
    rec = csv.writer(ds)
    #for i in dataSet:
    #    rec.writerow(i)
    rec.writerows(dataSet)

terraSet = []
with open("statester.csv", "r", newline="") as amerTer:
    terra = csv.DictReader(amerTer)
    for i in terra:
        terraSet.append((i["name"],i["Area"]))
print(terraSet)
